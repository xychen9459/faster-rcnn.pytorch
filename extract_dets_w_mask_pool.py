# --------------------------------------------------------
# Tensorflow Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Jiasen Lu, Jianwei Yang, based on code from Ross Girshick
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths
import os
import sys
import numpy as np
import argparse
import pprint
import pdb
import time
import cv2
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import pickle
from roi_data_layer.roidb import combined_roidb
from roi_data_layer.roibatchLoader import roibatchLoader
from model.utils.config import cfg, cfg_from_file, cfg_from_list, get_output_dir
from model.rpn.bbox_transform import clip_boxes
from model.nms.nms_wrapper import nms
from model.rpn.bbox_transform import bbox_transform_inv
from model.utils.net_utils import save_net, load_net, vis_detections
from model.faster_rcnn.vgg16 import vgg16
from model.faster_rcnn.resnet import resnet

import pdb

from matplotlib import pyplot as plt

try:
    xrange          # Python 2
except NameError:
    xrange = range  # Python 3


def parse_args():
  """
  Parse input arguments
  """
  parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
  parser.add_argument('--dataset', dest='dataset',
                      help='training dataset',
                      default='pascal_voc', type=str)
  parser.add_argument('--cfg', dest='cfg_file',
                      help='optional config file',
                      default='cfgs/vgg16.yml', type=str)
  parser.add_argument('--net', dest='net',
                      help='vgg16, res50, res101, res152',
                      default='res101', type=str)
  parser.add_argument('--set', dest='set_cfgs',
                      help='set config keys', default=None,
                      nargs=argparse.REMAINDER)
  parser.add_argument('--load_dir', dest='load_dir',
                      help='directory to load models', default="models",
                      type=str)
  parser.add_argument('--cuda', dest='cuda',
                      help='whether use CUDA',
                      action='store_true')
  parser.add_argument('--ls', dest='large_scale',
                      help='whether use large imag scale',
                      action='store_true')
  parser.add_argument('--mGPUs', dest='mGPUs',
                      help='whether use multiple GPUs',
                      action='store_true')
  parser.add_argument('--cag', dest='class_agnostic',
                      help='whether perform class_agnostic bbox regression',
                      action='store_true')
  parser.add_argument('--parallel_type', dest='parallel_type',
                      help='which part of model to parallel, 0: all, 1: model before roi pooling',
                      default=0, type=int)
  parser.add_argument('--checksession', dest='checksession',
                      help='checksession to load model',
                      default=1, type=int)
  parser.add_argument('--checkepoch', dest='checkepoch',
                      help='checkepoch to load network',
                      default=1, type=int)
  parser.add_argument('--checkpoint', dest='checkpoint',
                      help='checkpoint to load network',
                      default=10021, type=int)
  parser.add_argument('--vis', dest='vis',
                      help='visualization mode',
                      action='store_true')
  parser.add_argument('--subroi', dest='subroi',
                      help='specify sub roi mode',
                      default=0, type=int)
  args = parser.parse_args()
  return args

lr = cfg.TRAIN.LEARNING_RATE
momentum = cfg.TRAIN.MOMENTUM
weight_decay = cfg.TRAIN.WEIGHT_DECAY

from model.roi_mask_pooling.modules.roi_mask_pool import _RoIMaskPooling

def fetch_pooled_feat_grad(module, grad_input, grad_output):
    module.gradcam_rois = grad_output[0].cpu()

def custom_forward(model, im_data, im_info, gt_boxes, num_boxes, sub_roi=1):
    batch_size = im_data.size(0)
    im_info = im_info.data
    gt_boxes = gt_boxes.data
    num_boxes = num_boxes.data

    base_feat = model.RCNN_base(im_data)
    rois, _, _, _ = model.RCNN_rpn(base_feat, im_info, gt_boxes, num_boxes)
    proposal_data = model.RCNN_proposal_target(rois, gt_boxes, num_boxes)
    rois, rois_label = proposal_data[0], proposal_data[1]

    if cfg.POOLING_MODE == 'align':
        pooled_feat = model.RCNN_roi_align(base_feat, rois.view(-1, 5))
    elif cfg.POOLING_MODE == 'pool':
        pooled_feat = model.RCNN_roi_pool(base_feat, rois.view(-1, 5))
    # elif cfg.POOLING_MODE == 'mask':
    #     pooled_feat = _RoIMaskPooling(cfg.POOLING_SIZE, cfg.POOLING_SIZE, 1.0/16.0)\
    #         (base_feat, rois.view(-1, 5))

    model.mask_pooling.half_part = sub_roi
    model.mask_pooling.need_return_rois = True
    pooled_feat_2 = model.mask_pooling(base_feat, rois.view(-1, 5))
    return_rois = model.mask_pooling.return_rois[:, :5]

    pooled_feat = model._head_to_tail(pooled_feat)
    bbox_pred = model.RCNN_bbox_pred(pooled_feat)
    cls_score = model.RCNN_cls_score(pooled_feat)
    cls_prob = F.softmax(cls_score, 1).view(batch_size, rois.size(1), -1)
    bbox_pred = bbox_pred.view(batch_size, rois.size(1), -1)

    pooled_feat_2 = model._head_to_tail(pooled_feat_2)
    cls_score_2 = model.RCNN_cls_score(pooled_feat_2)
    cls_prob_2 = F.softmax(cls_score_2, 1).view(batch_size, rois.size(1), -1)

    return rois, cls_prob, bbox_pred, cls_prob_2, return_rois, cls_score, rois_label

def vis_proposals(im, rois, mask_pool_ret_rois, rois_cls_prob, ret_rois_cls_prob, det_boxes):
    """Visual debugging of detections."""
    for i in range(np.minimum(5, rois.shape[0])):
        roi_bbox = tuple(int(np.round(x)) for x in rois[i, 1:])
        ret_roi_bbox = tuple(int(np.round(x)) for x in mask_pool_ret_rois[i, 1:])
        det_bbox = tuple(int(np.round(x)) for x in det_boxes[i])
        cv2.rectangle(im, roi_bbox[0:2], roi_bbox[2:], (0, 204, 0), 2)
        cv2.rectangle(im, ret_roi_bbox[0:2], ret_roi_bbox[2:], (204, 0, 0), 2)
        cv2.rectangle(im, det_bbox[0:2], det_bbox[2:], (0, 0, 204), 2)
        cv2.putText(im, '%s: %.3f' % (rois_cls_prob[1][i], rois_cls_prob[0][i]), (roi_bbox[0], roi_bbox[1] + 15),
                    cv2.FONT_HERSHEY_PLAIN, 1.2, (255, 255, 0), thickness=2)
        cv2.putText(im, '%s: %.3f' % (ret_rois_cls_prob[1][i], ret_rois_cls_prob[0][i]), (ret_roi_bbox[0], ret_roi_bbox[3] - 15),
                    cv2.FONT_HERSHEY_PLAIN, 1.2, (255, 0, 255), thickness=2)
    return im

def vis_proposals_single_one(im, rois, mask_pool_ret_rois, rois_cls_prob, ret_rois_cls_prob, det_boxes, title=''):
    """Visual debugging of detections."""
    for i in range(np.minimum(30, rois.shape[0])):
        # im_show = np.copy(im)
        im_show = im[i]
        roi_bbox = tuple(int(np.round(x)) for x in rois[i, 1:])
        ret_roi_bbox = tuple(int(np.round(x)) for x in mask_pool_ret_rois[i, 1:])
        # det_bbox = tuple(int(np.round(x)) for x in det_boxes[i])
        cv2.rectangle(im_show, roi_bbox[0:2], roi_bbox[2:], (0, 204, 0), 2)
        # cv2.rectangle(im_show, ret_roi_bbox[0:2], ret_roi_bbox[2:], (204, 0, 0), 2)
        # cv2.rectangle(im_show, det_bbox[0:2], det_bbox[2:], (0, 0, 204), 2)
        cv2.putText(im_show, '%s: %.3f' % (rois_cls_prob[1][i], rois_cls_prob[0][i]),
                    (roi_bbox[0], roi_bbox[1] + 15),
                    cv2.FONT_HERSHEY_PLAIN, 1.2, (255, 255, 0), thickness=2)
        # cv2.putText(im_show, '%s: %.3f' % (ret_rois_cls_prob[1][i], ret_rois_cls_prob[0][i]),
        #             (ret_roi_bbox[0], ret_roi_bbox[3] - 15),
        #             cv2.FONT_HERSHEY_PLAIN, 1.2, (255, 0, 255), thickness=2)
        im_show = cv2.cvtColor(im_show, cv2.COLOR_BGR2RGB)
        plt.imshow(im_show)
        plt.title(title + ' ' + rois_cls_prob[1][i])
        plt.show()

if __name__ == '__main__':

  args = parse_args()

  print('Called with args:')
  print(args)

  if torch.cuda.is_available() and not args.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")

  np.random.seed(cfg.RNG_SEED)
  if args.dataset == "pascal_voc":
      args.imdb_name = "voc_2007_trainval"
      args.imdbval_name = "voc_2007_test"
      args.set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
  elif args.dataset == "pascal_voc_0712":
      args.imdb_name = "voc_2007_trainval+voc_2012_trainval"
      args.imdbval_name = "voc_2007_test"
      args.set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
  elif args.dataset == "coco":
      args.imdb_name = "coco_2014_train+coco_2014_valminusminival"
      args.imdbval_name = "coco_2014_minival"
      args.set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
  elif args.dataset == "imagenet":
      args.imdb_name = "imagenet_train"
      args.imdbval_name = "imagenet_val"
      args.set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
  elif args.dataset == "vg":
      args.imdb_name = "vg_150-50-50_minitrain"
      args.imdbval_name = "vg_150-50-50_minival"
      args.set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']

  if args.cfg_file is None:  # use default cfg file
    args.cfg_file = "cfgs/{}_ls.yml".format(args.net) if args.large_scale else "cfgs/{}.yml".format(args.net)

  print('Using cfg file: %s' % args.cfg_file)
  cfg_from_file(args.cfg_file)

  if args.set_cfgs is not None:
    cfg_from_list(args.set_cfgs)

  print('Using config:')
  pprint.pprint(cfg)

  cfg.TRAIN.USE_FLIPPED = False
  imdb, roidb, ratio_list, ratio_index = combined_roidb(args.imdbval_name, False)
  imdb.competition_mode(on=True)
  all_classes = imdb.classes

  print('{:d} roidb entries'.format(len(roidb)))

  input_dir = args.load_dir + "/" + args.net + "/" + args.dataset
  if not os.path.exists(input_dir):
    raise Exception('There is no input directory for loading network from ' + input_dir)
  load_name = os.path.join(input_dir,
    'faster_rcnn_{}_{}_{}.pth'.format(args.checksession, args.checkepoch, args.checkpoint))

  # initilize the network here.
  if args.net == 'vgg16':
    fasterRCNN = vgg16(imdb.classes, pretrained=False, class_agnostic=args.class_agnostic)
  elif args.net == 'res101':
    fasterRCNN = resnet(imdb.classes, 101, pretrained=False, class_agnostic=args.class_agnostic)
  elif args.net == 'res50':
    fasterRCNN = resnet(imdb.classes, 50, pretrained=False, class_agnostic=args.class_agnostic)
  elif args.net == 'res152':
    fasterRCNN = resnet(imdb.classes, 152, pretrained=False, class_agnostic=args.class_agnostic)
  else:
    print("network is not defined")
    pdb.set_trace()

  fasterRCNN.create_architecture()
  fasterRCNN.mask_pooling = _RoIMaskPooling(cfg.POOLING_SIZE, cfg.POOLING_SIZE, 1.0 / 16.0)
  if cfg.POOLING_MODE == 'align':
      fasterRCNN.RCNN_roi_align.register_backward_hook(fetch_pooled_feat_grad)
  elif cfg.POOLING_MODE == 'pool':
      fasterRCNN.RCNN_roi_pool.register_backward_hook(fetch_pooled_feat_grad)


  print("load checkpoint %s" % (load_name))
  checkpoint = torch.load(load_name)
  fasterRCNN.load_state_dict(checkpoint['model'])
  if 'pooling_mode' in checkpoint.keys():
    cfg.POOLING_MODE = checkpoint['pooling_mode']
  # pdb.set_trace()


  print('load model successfully!')
  # initilize the tensor holder here.
  im_data = torch.FloatTensor(1)
  im_info = torch.FloatTensor(1)
  num_boxes = torch.LongTensor(1)
  gt_boxes = torch.FloatTensor(1)

  # ship to cuda
  if args.cuda:
    im_data = im_data.cuda()
    im_info = im_info.cuda()
    num_boxes = num_boxes.cuda()
    gt_boxes = gt_boxes.cuda()

  # make variable
  im_data = Variable(im_data)
  im_info = Variable(im_info)
  num_boxes = Variable(num_boxes)
  gt_boxes = Variable(gt_boxes)

  if args.cuda:
    cfg.CUDA = True

  if args.cuda:
    fasterRCNN.cuda()

  bbox_norm_stds = torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_STDS)
  bbox_norm_means = torch.FloatTensor(cfg.TRAIN.BBOX_NORMALIZE_MEANS)
  if args.cuda:
      bbox_norm_stds = bbox_norm_stds.cuda()
      bbox_norm_means = bbox_norm_means.cuda()

  start = time.time()
  max_per_image = 100

  vis = args.vis

  if vis:
    thresh = 0.05
  else:
    thresh = 0.0

  save_name = 'faster_rcnn_mask_roipooling_subroi_' + str(args.subroi)
  num_images = len(imdb.image_index)
  all_boxes = [[[] for _ in xrange(num_images)]
               for _ in xrange(imdb.num_classes)]

  output_dir = get_output_dir(imdb, save_name)
  dataset = roibatchLoader(roidb, ratio_list, ratio_index, 1, \
                        imdb.num_classes, training=False, normalize = False)
  dataloader = torch.utils.data.DataLoader(dataset, batch_size=1,
                            shuffle=False, num_workers=0,
                            pin_memory=True)

  data_iter = iter(dataloader)

  _t = {'im_detect': time.time(), 'misc': time.time()}

  fasterRCNN.eval()
  empty_array = np.transpose(np.array([[],[],[],[],[]]), (1,0))

  for i in range(num_images):

      data = next(data_iter)
      im_data.data.resize_(data[0].size()).copy_(data[0])
      im_info.data.resize_(data[1].size()).copy_(data[1])
      gt_boxes.data.resize_(data[2].size()).copy_(data[2])
      num_boxes.data.resize_(data[3].size()).copy_(data[3])

      det_tic = time.time()
      rois, cls_prob, bbox_pred, cls_prob_2, return_rois, cls_score, roi_labels = \
          custom_forward(fasterRCNN, im_data, im_info, gt_boxes, num_boxes, args.subroi)
      scores = cls_prob.data
      boxes = rois.data[:, :, 1:5]

      cls_prob_cpu = cls_prob.detach().squeeze().cpu()
      cls_prob_2_cpu = cls_prob_2.detach().squeeze().cpu()

      if cfg.TEST.BBOX_REG:
          # Apply bounding-box regression deltas
          box_deltas = bbox_pred.data
          if cfg.TRAIN.BBOX_NORMALIZE_TARGETS_PRECOMPUTED:
          # Optionally normalize targets by a precomputed mean and stdev
            if args.class_agnostic:
                box_deltas = box_deltas.view(-1, 4) * bbox_norm_stds + bbox_norm_means
                box_deltas = box_deltas.view(1, -1, 4)
            else:
                box_deltas = box_deltas.view(-1, 4) * bbox_norm_stds + bbox_norm_means
                box_deltas = box_deltas.view(1, -1, 4 * len(imdb.classes))

          pred_boxes = bbox_transform_inv(boxes, box_deltas, 1)
          pred_boxes = clip_boxes(pred_boxes, im_info.data, 1)
      else:
          # Simply repeat the boxes, once for each class
          pred_boxes = np.tile(boxes, (1, scores.shape[1]))

      # all boxes (predictions, rois, anchors) need to be resized to original sizes
      pred_boxes /= im_info[0][2].item()

      scores = scores.squeeze()
      pred_boxes = pred_boxes.squeeze()
      det_toc = time.time()
      detect_time = det_toc - det_tic
      misc_tic = time.time()

      if vis:
          im = cv2.imread(imdb.image_path_at(i))
          rois_resized = (rois / im_info[0][2].item()).squeeze()
          return_rois_resized = (return_rois / im_info[0][2].item()).squeeze()
          roi_labels_cpu = roi_labels.cpu().long().squeeze()
          rois_max_prob, rois_max_cls_idx = torch.max(cls_prob_cpu, 1)
          return_rois_max_prob, return_rois_max_cls_idx = torch.max(cls_prob_2_cpu, 1)
          rois_max_cls = [all_classes[i] if i > 0 else 'bg' for i in roi_labels_cpu]
          rois_max_prob = cls_prob_cpu.gather(dim=1, index=roi_labels_cpu.unsqueeze(1))
          return_rois_max_cls = [all_classes[i] if i > 0 else 'bg' for i in return_rois_max_cls_idx]
          det_max_cls_boxes_idx = torch.cat([torch.tensor([0, 1, 2, 3]).unsqueeze(0)+(4*i)
                                             for i in rois_max_cls_idx], dim=0)
          pred_boxes_cpu = pred_boxes.cpu()
          det_max_cls_boxes = torch.gather(pred_boxes_cpu, dim=1, index=det_max_cls_boxes_idx)

          # im = vis_proposals(im, rois_resized, return_rois_resized,
          #                    (rois_max_prob, rois_max_cls), (return_rois_max_prob, return_rois_max_cls),
          #                    det_max_cls_boxes)
          # cv2.imwrite(os.path.join(output_dir, imdb.image_index[int(roidb[i]['img_id'])] + '_maskroi.png'),
          #             im)
          # plt.imshow(im)
          # plt.title(imdb.image_index[int(roidb[i]['img_id'])])
          # plt.show()

          nrois = len(rois_max_cls_idx)
          nclasses = len(all_classes)
          gradcam_in_grads = torch.zeros(nrois, nclasses)
          gradcam_in_grads.scatter_(dim=1, index=roi_labels_cpu.unsqueeze(1), src=torch.ones(nrois, 1))

          if args.cuda:
              gradcam_in_grads = gradcam_in_grads.cuda()

          fasterRCNN.zero_grad()
          # cls_prob.backward(gradient=gradcam_in_grads.unsqueeze(0), retain_graph=True)
          cls_score.backward(gradient=gradcam_in_grads, retain_graph=True)

          if cfg.POOLING_MODE == 'align':
              w = F.adaptive_avg_pool2d(fasterRCNN.RCNN_roi_align.gradcam_rois, 1)
              a = F.relu(torch.sum(fasterRCNN.RCNN_roi_align.gradcam_rois * w, 1))
          elif cfg.POOLING_MODE == 'pool':
              w = F.adaptive_avg_pool2d(fasterRCNN.RCNN_roi_pool.gradcam_rois, 1)
              a = F.relu(torch.sum(fasterRCNN.RCNN_roi_pool.gradcam_rois * w, 1))

          a_min = torch.min(a.view(nrois, -1), dim=1, keepdim=True)[0].unsqueeze(2)
          a -= a_min
          a_max = torch.max(a.view(nrois, -1), dim=1, keepdim=True)[0].unsqueeze(2)
          a /= a_max
          a = a.cpu().numpy()

          roi_cpu = rois.cpu().numpy()[0]
          gradcam_im = []
          for i in range(nrois):
              roi_x1 = int(np.floor(roi_cpu[i][1]))
              roi_y1 = int(np.floor(roi_cpu[i][2]))
              roi_x2 = int(np.ceil(roi_cpu[i][3]))
              roi_y2 = int(np.ceil(roi_cpu[i][4]))

              roi_w = roi_x2 - roi_x1
              roi_h = roi_y2 - roi_y1

              if roi_w == 0. or roi_h == 0.:
                  continue
              # print("roi_w, roi_h = %.3f, %.3f" % (roi_w, roi_h))
              gcam = np.zeros((int(im_info[0][0]), int(im_info[0][1])), dtype=np.float32)
              gradcam_resize = cv2.resize(a[i], (roi_w, roi_h))
              gcam[roi_y1:roi_y2, roi_x1:roi_x2] = gradcam_resize
              gcam = cv2.applyColorMap(np.uint8(gcam * 255.0), cv2.COLORMAP_JET)
              gcam = cv2.resize(gcam, (im.shape[1], im.shape[0]))
              gcam = gcam.astype(np.float) + im.astype(np.float)
              gcam = gcam / gcam.max() * 255.0
              gradcam_im.append(np.uint8(gcam))

          vis_proposals_single_one(gradcam_im, rois_resized, return_rois_resized,
                        (rois_max_prob, rois_max_cls), (return_rois_max_prob, return_rois_max_cls),
                        det_max_cls_boxes, title=imdb.image_index[int(roidb[i]['img_id'])])

      for j in xrange(1, imdb.num_classes):
          inds = torch.nonzero(scores[:,j]>thresh).view(-1)
          # if there is det
          if inds.numel() > 0:
            cls_scores = scores[:,j][inds]
            _, order = torch.sort(cls_scores, 0, True)
            if args.class_agnostic:
              cls_boxes = pred_boxes[inds, :]
            else:
              cls_boxes = pred_boxes[inds][:, j * 4:(j + 1) * 4]
            
            cls_dets = torch.cat((cls_boxes, cls_scores.unsqueeze(1)), 1)
            cls_dets = cls_dets[order]

            keep = nms(cls_dets, cfg.TEST.NMS, force_cpu=not cfg.USE_GPU_NMS)
            cls_dets = cls_dets[keep.view(-1).long()]

      misc_toc = time.time()
      nms_time = misc_toc - misc_tic
      print('im_detect: {:d}/{:d} {:.3f}s {:.3f}s' \
            .format(i + 1, num_images, detect_time, nms_time))


  end = time.time()
  print("extract time: %0.4fs" % (end - start))
