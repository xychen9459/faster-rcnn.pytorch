# https://github.com/open-mmlab/mmdetection/blob/master/mmdet/ops/roi_pool/gradcheck.py

import torch
from torch.autograd import gradcheck

import os.path as osp
import sys

script_path = osp.realpath(__file__)
sys.path.insert(0, osp.join(osp.split(script_path)[0], '../'))
from roi_pooling.functions.roi_pool import RoIPoolFunction

feat = torch.randn(2, 3, 100, 100, requires_grad=True).cuda()
rois = torch.Tensor([[0, 0, 0, 50, 50], [1, 10, 30, 43, 55]]).cuda()
inputs = (feat, rois)
print('Gradcheck for roi pooling...')
test = gradcheck(RoIPoolFunction(4, 4, 1.0 / 8), inputs, eps=1e-5, atol=1e-3)
print(test)

if __name__ == "__main__":
    pass