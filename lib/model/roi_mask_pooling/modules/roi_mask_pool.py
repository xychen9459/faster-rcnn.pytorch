from torch.nn.modules.module import Module
from ..functions.roi_mask_pool import RoIMaskPoolFunction


class _RoIMaskPooling(Module):
    def __init__(self,
                 pooled_height, pooled_width,
                 spatial_scale, spatial_shift=0.0,
                 half_part=0,
                 roi_scale=1.0, mask_scale=0.0,
                 need_return_rois=False):
        super(_RoIMaskPooling, self).__init__()

        self.pooled_width = int(pooled_width)
        self.pooled_height = int(pooled_height)
        self.spatial_scale = float(spatial_scale)
        self.spatial_shift = float(spatial_shift)
        self.half_part = int(half_part)
        self.roi_scale = float(roi_scale)
        self.mask_scale = float(mask_scale)
        self.need_return_rois = need_return_rois
        self.return_rois = None

    def forward(self, features, rois):
        f = RoIMaskPoolFunction(self.pooled_height, self.pooled_width,
                                self.spatial_scale, self.spatial_shift,
                                self.half_part,
                                self.roi_scale, self.mask_scale,
                                self.need_return_rois)
        out = f(features, rois)
        if self.need_return_rois:
            self.return_rois = f.return_rois
        return out
