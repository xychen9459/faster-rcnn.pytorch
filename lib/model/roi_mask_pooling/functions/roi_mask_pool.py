import torch
from torch.autograd import Function
from .._ext import roi_mask_pooling
import pdb

class RoIMaskPoolFunction(Function):
    def __init__(ctx,
                 pooled_height, pooled_width,
                 spatial_scale, spatial_shift = 0.0,
                 half_part = 0,
                 roi_scale = 1.0, mask_scale = 0.0,
                 need_return_rois = False):
        ctx.pooled_width = pooled_width
        ctx.pooled_height = pooled_height
        ctx.spatial_scale = spatial_scale
        ctx.spatial_shift = spatial_shift
        ctx.half_part = half_part
        ctx.roi_scale = roi_scale
        ctx.mask_scale = mask_scale
        ctx.feature_size = None
        ctx.need_return_rois = need_return_rois

    def forward(ctx, features, rois): 
        ctx.feature_size = features.size()           
        batch_size, num_channels, data_height, data_width = ctx.feature_size
        num_rois = rois.size(0)
        output = features.new(num_rois, num_channels, ctx.pooled_height, ctx.pooled_width).zero_()
        ctx.return_rois = torch.tensor([])
        if ctx.need_return_rois:
            ctx.return_rois = rois.new(num_rois, 9).zero_()
        ctx.argmax = features.new(num_rois, num_channels, ctx.pooled_height, ctx.pooled_width).zero_().int()
        ctx.rois = rois
        if not features.is_cuda:
            _features = features.permute(0, 2, 3, 1)
            roi_mask_pooling.roi_mask_pooling_forward(ctx.pooled_height, ctx.pooled_width,
                                                      ctx.spatial_scale, ctx.spatial_shift,
                                                      ctx.half_part,
                                                      ctx.roi_scale, ctx.mask_scale,
                                                      _features, rois, output, ctx.return_rois)
        else:
            ctx.return_rois = ctx.return_rois.cuda()
            roi_mask_pooling.roi_mask_pooling_forward_cuda(ctx.pooled_height, ctx.pooled_width,
                                                           ctx.spatial_scale, ctx.spatial_shift,
                                                           ctx.half_part,
                                                           ctx.roi_scale, ctx.mask_scale,
                                                           features, rois, output, ctx.argmax, ctx.return_rois)

        return output

    def backward(ctx, grad_output):
        assert(ctx.feature_size is not None and grad_output.is_cuda)
        batch_size, num_channels, data_height, data_width = ctx.feature_size
        grad_input = grad_output.new(batch_size, num_channels, data_height, data_width).zero_()

        roi_mask_pooling.roi_mask_pooling_backward_cuda(ctx.pooled_height, ctx.pooled_width,
                                                        ctx.spatial_scale, ctx.spatial_shift,
                                                        ctx.half_part,
                                                        ctx.roi_scale, ctx.mask_scale,
                                                        grad_output, ctx.rois, grad_input, ctx.argmax)

        return grad_input, None
