# https://github.com/open-mmlab/mmdetection/blob/master/mmdet/ops/roi_pool/gradcheck.py
# https://discuss.pytorch.org/t/why-is-nn-linear-failing-gradcheck-is-there-a-bug-in-gradcheck/17346/2

import torch
from torch.autograd import gradcheck

import os.path as osp
import sys

script_path = osp.realpath(__file__)
sys.path.insert(0, osp.join(osp.split(script_path)[0], '../'))
from roi_mask_pooling.functions.roi_mask_pool import RoIMaskPoolFunction

feat = torch.randn(2, 3, 200, 200, requires_grad=True).float().cuda()
rois = torch.Tensor([[0, 0, 0, 50, 50], [1, 10, 30, 43, 55]]).float().cuda()
inputs = (feat, rois)
print('Gradcheck for roi mask pooling...')

feat_2 = feat.cpu()
rois_2 = rois.cpu()
f = RoIMaskPoolFunction(4, 4, 1.0, half_part=1)
f(feat_2, rois_2)

test = gradcheck(RoIMaskPoolFunction(4, 4, 1.0/8, 25.0), inputs, eps=1e-5, atol=1e-3)
print(test)

if __name__ == "__main__":
    pass