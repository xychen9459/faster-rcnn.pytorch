#include <TH/TH.h>
#include <math.h>

int roi_mask_pooling_forward(int pooled_height, int pooled_width, float spatial_scale,
                        float spatial_shift, int half_part, float roi_scale, float mask_scale,
                        THFloatTensor * features, THFloatTensor * rois, THFloatTensor * output,
                        THFloatTensor * return_rois)
{
    // Grab the input tensor
    float * data_flat = THFloatTensor_data(features);
    float * rois_flat = THFloatTensor_data(rois);

    float * output_flat = THFloatTensor_data(output);

    float * return_rois_flat = THFloatTensor_data(return_rois);
    char need_return_rois = (THFloatTensor_nDimension(return_rois) != 0);

    // Number of ROIs
    int num_rois = THFloatTensor_size(rois, 0);
    int size_rois = THFloatTensor_size(rois, 1);
    int size_return_rois = 0;
    if (need_return_rois) {
        size_return_rois = THFloatTensor_size(return_rois, 1);
    }
    // batch size
    int batch_size = THFloatTensor_size(features, 0);
    if(batch_size != 1)
    {
        //return 0;
    }
    // data height
    int data_height = THFloatTensor_size(features, 1);
    // data width
    int data_width = THFloatTensor_size(features, 2);
    // Number of channels
    int num_channels = THFloatTensor_size(features, 3);

    // Set all element of the output tensor to -inf.
    THFloatStorage_fill(THFloatTensor_storage(output), -1);

    // For each ROI R = [batch_index x1 y1 x2 y2]: max pool over R
    int index_roi = 0;
    int index_return_roi = 0;
    int index_output = 0;
    int n;
    for (n = 0; n < num_rois; ++n)
    {
        // batch index
        int roi_batch_ind = rois_flat[index_roi + 0];

        // [x1, y1, x2, y2]
        float x1 = rois_flat[index_roi + 1];
        float y1 = rois_flat[index_roi + 2];
        float x2 = rois_flat[index_roi + 3];
        float y2 = rois_flat[index_roi + 4];

        // [x1, y1, x2, y2] -> [xc, yc, w, h]
        float xc = (x1 + x2) / 2;
        float yc = (y1 + y2) / 2;
        float w = x2 - x1;
        float h = y2 - y1;

        // rescale roi with regard to roi_scale and half_part
        float xx1 = xc - w * roi_scale / 2.0;
        float yy1 = yc - h * roi_scale / 2.0;
        float xx2 = xc + w * roi_scale / 2.0;
        float yy2 = yc + h * roi_scale / 2.0;

        // select subregion in roi
        switch (half_part) {
            case 0: break;
            case 1: xx2 = xc; break;  // left-half
            case 2: xx1 = xc; break;  // right-half
            case 3: yy2 = yc; break;  // top-half
            case 4: yy1 = yc; break;  // bottom-half
            case 5: xx2 = xc; yy2 = yc; break;  // left-top
            case 6: xx2 = xc; yy1 = yc; break;  // left-bottom
            case 7: xx1 = xc; yy2 = yc; break;  // right-top
            case 8: xx1 = xc; yy1 = yc; break;  // right-bottom
            default: break;
        }

        // rescale mask with regard to mask_scale
        char isMask = mask_scale > 0.0;
        float mx1 = xc - w * mask_scale / 2.0;
        float my1 = yc - h * mask_scale / 2.0;
        float mx2 = xc + w * mask_scale / 2.0;
        float my2 = yc + h * mask_scale / 2.0;

        // rescaled roi/mask size on conv featmap
        int roi_start_w = round(xx1 * spatial_scale + spatial_shift);
        int roi_start_h = round(yy1 * spatial_scale + spatial_shift);
        int roi_end_w = round(xx2 * spatial_scale + spatial_shift);
        int roi_end_h = round(yy2 * spatial_scale + spatial_shift);

        int mask_start_w = round(mx1 * spatial_scale + spatial_shift);
        int mask_start_h = round(my1 * spatial_scale + spatial_shift);
        int mask_end_w = round(mx2 * spatial_scale + spatial_shift);
        int mask_end_h = round(my2 * spatial_scale + spatial_shift);

        // return mask and roi before 'spatial_scale' and 'spatial_shift'
        if (need_return_rois) {
            return_rois_flat[index_return_roi + 0] = (float)roi_batch_ind;
            return_rois_flat[index_return_roi + 1] = (float)xx1;
            return_rois_flat[index_return_roi + 2] = (float)yy1;
            return_rois_flat[index_return_roi + 3] = (float)xx2;
            return_rois_flat[index_return_roi + 4] = (float)yy2;
            return_rois_flat[index_return_roi + 5] = 0.0;
            return_rois_flat[index_return_roi + 6] = 0.0;
            return_rois_flat[index_return_roi + 7] = 0.0;
            return_rois_flat[index_return_roi + 8] = 0.0;
            if (isMask) {
                return_rois_flat[index_return_roi + 5] = (float)mx1;
                return_rois_flat[index_return_roi + 6] = (float)my1;
                return_rois_flat[index_return_roi + 7] = (float)mx2;
                return_rois_flat[index_return_roi + 8] = (float)my2;
            }
        }

        // Force malformed ROIs to be 1x1
        int roi_height = fmaxf(roi_end_h - roi_start_h + 1, 1);
        int roi_width = fmaxf(roi_end_w - roi_start_w + 1, 1);
        float bin_size_h = (float)(roi_height) / (float)(pooled_height);
        float bin_size_w = (float)(roi_width) / (float)(pooled_width);

        int index_data = roi_batch_ind * data_height * data_width * num_channels;
        const int output_area = pooled_width * pooled_height;

        int c, ph, pw;
        for (ph = 0; ph < pooled_height; ++ph) {
            for (pw = 0; pw < pooled_width; ++pw) {
                int hstart = (floor((float)(ph) * bin_size_h));
                int wstart = (floor((float)(pw) * bin_size_w));
                int hend = (ceil((float)(ph + 1) * bin_size_h));
                int wend = (ceil((float)(pw + 1) * bin_size_w));

                hstart = fminf(fmaxf(hstart + roi_start_h, 0), data_height);
                hend = fminf(fmaxf(hend + roi_start_h, 0), data_height);
                wstart = fminf(fmaxf(wstart + roi_start_w, 0), data_width);
                wend = fminf(fmaxf(wend + roi_start_w, 0), data_width);

                const int pool_index = index_output + (ph * pooled_width + pw);
                int is_empty = (hend <= hstart) || (wend <= wstart);
                if (is_empty) {
                    for (c = 0; c < num_channels * output_area; c += output_area) {
                        output_flat[pool_index + c] = 0;
                    }
                }
                else {
                    int h, w, c;
                    for (h = hstart; h < hend; ++h) {
                        for (w = wstart; w < wend; ++w) {
                            for (c = 0; c < num_channels; ++c) {
                                const int index = (h * data_width + w) * num_channels + c;
                                float value = data_flat[index_data + index];
                                if (isMask) {
                                    if (w >= mask_start_w && w <= mask_end_w
                                          && h >= mask_start_h && h <= mask_end_h) {
                                        value = 0;
                                    }
                                }
                                if (value> output_flat[pool_index + c * output_area]) {
                                    output_flat[pool_index + c * output_area] = value;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Increment ROI index
        index_roi += size_rois;
        if (need_return_rois) {
            index_return_roi += size_return_rois;
        }
        index_output += pooled_height * pooled_width * num_channels;
    }
    return 1;
}