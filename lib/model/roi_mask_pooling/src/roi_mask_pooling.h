int roi_mask_pooling_forward(int pooled_height, int pooled_width,
                             float spatial_scale, float spatial_shift,
                             int half_part,
                             float roi_scale, float mask_scale,
                             THFloatTensor * features, THFloatTensor * rois, THFloatTensor * output,
                             THFloatTensor * return_rois);