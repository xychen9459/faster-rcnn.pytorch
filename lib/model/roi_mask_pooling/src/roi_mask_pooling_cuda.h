int roi_mask_pooling_forward_cuda(int pooled_height, int pooled_width,
                                  float spatial_scale, float spatial_shift,
                                  int half_part,
                                  float roi_scale, float mask_scale,
                                  THCudaTensor * features, THCudaTensor * rois,
                                  THCudaTensor * output, THCudaIntTensor * argmax,
                                  THCudaTensor * return_rois);

int roi_mask_pooling_backward_cuda(int pooled_height, int pooled_width,
                                   float spatial_scale, float spatial_shift,
                                   int half_part,
                                   float roi_scale, float mask_scale,
                                   THCudaTensor * top_grad, THCudaTensor * rois,
                                   THCudaTensor * bottom_grad, THCudaIntTensor * argmax);
