#ifndef _ROI_MASK_POOLING_KERNEL
#define _ROI_MASK_POOLING_KERNEL

#ifdef __cplusplus
extern "C" {
#endif

int ROIMaskPoolForwardLaucher(cudaStream_t stream,
                        const float* bottom_data, const float* bottom_rois, float* top_data, int* argmax_data,
                        float* return_rois,
                        const int height, const int width, const int channels,
                        const int pooled_height, const int pooled_width, const int num_rois,
                        const float roi_scale, const float mask_scale, const int half_part,
                        const float spatial_scale, const float spatial_shift);

int ROIMaskPoolBackwardLaucher(cudaStream_t stream,
                        float* bottom_diff, const float* bottom_rois, const float* top_diff, const int* argmax_data,
                        const int height, const int width, const int channels, const int batch_size,
                        const int pooled_height, const int pooled_width, const int num_rois,
                        const float roi_scale, const float mask_scale, const int half_part,
                        const float spatial_scale, const float spatial_shift);

#ifdef __cplusplus
}
#endif

#endif
