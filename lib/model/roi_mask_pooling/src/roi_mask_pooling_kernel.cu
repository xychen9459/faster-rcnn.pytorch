// #ifdef __cplusplus
// extern "C" {
// #endif

#include <stdio.h>
#include <vector>
#include <math.h>
#include <float.h>
#include "roi_mask_pooling_kernel.h"


#define CUDA_1D_KERNEL_LOOP(i, n)                            \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < n; \
       i += blockDim.x * gridDim.x)

// CUDA: grid stride looping
#define CUDA_KERNEL_LOOP(i, n) \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
       i < (n); \
       i += blockDim.x * gridDim.x)


__global__ void ROIMaskPoolForward(const int nthreads,
                               const float* bottom_data, const float* bottom_rois, float* top_data, int* argmax_data,
                               float* return_rois,
                               const int height, const int width, const int channels,
                               const int pooled_height, const int pooled_width,
                               const float roi_scale, const float mask_scale, const int half_part,
                               const float spatial_scale, const float spatial_shift) {

    CUDA_KERNEL_LOOP(index, nthreads)
    {
        // (n, c, ph, pw) is an element in the pooled output
        int pw = index % pooled_width;
        int ph = (index / pooled_width) % pooled_height;
        int c  = (index / pooled_width / pooled_height) % channels;
        int n  = index / pooled_width / pooled_height / channels;

        // bottom_rois += n * 5;
        int roi_batch_ind = bottom_rois[n * 5 + 0];

        // [x1, y1, x2, y2]
        float x1 = bottom_rois[n * 5 + 1];
        float y1 = bottom_rois[n * 5 + 2];
        float x2 = bottom_rois[n * 5 + 3];
        float y2 = bottom_rois[n * 5 + 4];

        // [x1, y1, x2, y2] -> [xc, yc, w, h]
        float xc = (x1 + x2) / 2.0;
        float yc = (y1 + y2) / 2.0;
        float w = x2 - x1;
        float h = y2 - y1;

        // rescale roi with regard to roi_scale and half_part
        float xx1 = xc - w * roi_scale / 2.0;
        float yy1 = yc - h * roi_scale / 2.0;
        float xx2 = xc + w * roi_scale / 2.0;
        float yy2 = yc + h * roi_scale / 2.0;

        // select subregion in roi
        switch (half_part) {
            case 0: break;
            case 1: xx2 = xc; break;  // left-half
            case 2: xx1 = xc; break;  // right-half
            case 3: yy2 = yc; break;  // top-half
            case 4: yy1 = yc; break;  // bottom-half
            case 5: xx2 = xc; yy2 = yc; break;  // left-top
            case 6: xx2 = xc; yy1 = yc; break;  // left-bottom
            case 7: xx1 = xc; yy2 = yc; break;  // right-top
            case 8: xx1 = xc; yy1 = yc; break;  // right-bottom
            default: break;
        }

        // rescale mask with regard to mask_scale
        bool isMask = mask_scale > 0.0;
        float mx1 = xc - w * mask_scale / 2.0;
        float my1 = yc - h * mask_scale / 2.0;
        float mx2 = xc + w * mask_scale / 2.0;
        float my2 = yc + h * mask_scale / 2.0;

        // rescaled roi/mask size on conv featmap
        int roi_start_w = round(xx1 * spatial_scale + spatial_shift);
        int roi_start_h = round(yy1 * spatial_scale + spatial_shift);
        int roi_end_w = round(xx2 * spatial_scale + spatial_shift);
        int roi_end_h = round(yy2 * spatial_scale + spatial_shift);

        int mask_start_w = round(mx1 * spatial_scale + spatial_shift);
        int mask_start_h = round(my1 * spatial_scale + spatial_shift);
        int mask_end_w = round(mx2 * spatial_scale + spatial_shift);
        int mask_end_h = round(my2 * spatial_scale + spatial_shift);

        // return mask and roi before 'spatial_scale' and 'spatial_shift'
        if (return_rois != NULL) {
            return_rois[n * 9 + 0] = (float)roi_batch_ind;
            return_rois[n * 9 + 1] = (float)xx1;
            return_rois[n * 9 + 2] = (float)yy1;
            return_rois[n * 9 + 3] = (float)xx2;
            return_rois[n * 9 + 4] = (float)yy2;
            return_rois[n * 9 + 5] = 0.0;
            return_rois[n * 9 + 6] = 0.0;
            return_rois[n * 9 + 7] = 0.0;
            return_rois[n * 9 + 8] = 0.0;
            if (isMask) {
                return_rois[n * 9 + 5] = (float)mx1;
                return_rois[n * 9 + 6] = (float)my1;
                return_rois[n * 9 + 7] = (float)mx2;
                return_rois[n * 9 + 8] = (float)my2;
            }
        }

        // Force malformed ROIs to be 1x1
        int roi_width = fmaxf(roi_end_w - roi_start_w + 1, 1);
        int roi_height = fmaxf(roi_end_h - roi_start_h + 1, 1);
        float bin_size_h = (float)(roi_height) / (float)(pooled_height);
        float bin_size_w = (float)(roi_width) / (float)(pooled_width);

        int hstart = (int)(floor((float)(ph) * bin_size_h));
        int wstart = (int)(floor((float)(pw) * bin_size_w));
        int hend = (int)(ceil((float)(ph + 1) * bin_size_h));
        int wend = (int)(ceil((float)(pw + 1) * bin_size_w));

        // Add roi offsets and clip to input boundaries
        hstart = fminf(fmaxf(hstart + roi_start_h, 0), height);
        hend = fminf(fmaxf(hend + roi_start_h, 0), height);
        wstart = fminf(fmaxf(wstart + roi_start_w, 0), width);
        wend = fminf(fmaxf(wend + roi_start_w, 0), width);
        bool is_empty = (hend <= hstart) || (wend <= wstart);

        // Define an empty pooling region to be zero
        float maxval = is_empty ? 0 : -FLT_MAX;
        // If nothing is pooled, argmax = -1 causes nothing to be backprop'd
        int maxidx = -1;

        int bottom_data_batch_offset = roi_batch_ind * channels * height * width;
        int bottom_data_offset = bottom_data_batch_offset + c * height * width;

        for (int h = hstart; h < hend; ++h) {
            for (int w = wstart; w < wend; ++w) {
                // int bottom_index = (h * width + w) * channels + c;
                // int bottom_index = (c * height + h) * width + w;
                int bottom_index = h * width + w;
                float value = bottom_data[bottom_data_offset + bottom_index];
                if (isMask) {
                    if (w >= mask_start_w && w <= mask_end_w
                         && h >= mask_start_h && h <= mask_end_h) {
                       value = 0;
                    }
                }
                if (value > maxval) {
                    maxval = value;
                    maxidx = bottom_data_offset + bottom_index;
                }
            }
        }
        top_data[index] = maxval;
        if (argmax_data != NULL)
            argmax_data[index] = maxidx;
    }
}

int ROIMaskPoolForwardLaucher(cudaStream_t stream,
                          const float* bottom_data, const float* bottom_rois, float* top_data, int* argmax_data,
                          float* return_rois,
                          const int height, const int width, const int channels,
                          const int pooled_height, const int pooled_width, const int num_rois,
                          const float roi_scale, const float mask_scale, const int half_part,
                          const float spatial_scale, const float spatial_shift) {

    const int kThreadsPerBlock = 1024;
    int output_size = num_rois * pooled_height * pooled_width * channels;
    cudaError_t err;

    ROIMaskPoolForward<<<(output_size + kThreadsPerBlock - 1) / kThreadsPerBlock, kThreadsPerBlock, 0, stream>>>(
          output_size,
          bottom_data, bottom_rois, top_data, argmax_data,
          return_rois,
          height, width, channels,
          pooled_height, pooled_width,
          roi_scale, mask_scale, half_part,
          spatial_scale, spatial_shift);

    err = cudaGetLastError();
    if(cudaSuccess != err) {
        fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
        exit( -1 );
    }

    return 1;
}


__global__ void ROIMaskPoolBackward(const int nthreads,
                         const float* top_diff, const int* argmax_data, float* bottom_diff, const float* bottom_rois,
                         const int height, const int width, const int channels,
                         const int pooled_height, const int pooled_width,
                         const float roi_scale, const float mask_scale, const int half_part,
                         const float spatial_scale, const float spatial_shift,
                         const int num_rois) {
    CUDA_1D_KERNEL_LOOP(index, nthreads)
    {
        // (n, c, h, w) is an element in the bottom conv feat
        int w = index % width;
        int h = (index / width) % height;
        int c  = (index / width / height) % channels;
        int n  = index / width / height / channels;

        float gradient = 0;
        // Accumulate gradient over all ROIs that pooled this element
        for (int roi_n = 0; roi_n < num_rois; ++roi_n)
        {
            const float* offset_bottom_rois = bottom_rois + roi_n * 5;
            int roi_batch_ind = offset_bottom_rois[0];
            // Skip if ROI's batch index doesn't match n
            if (n != roi_batch_ind) {
                continue;
            }

            // [x1, y1, x2, y2]
            float x1 = offset_bottom_rois[1];
            float y1 = offset_bottom_rois[2];
            float x2 = offset_bottom_rois[3];
            float y2 = offset_bottom_rois[4];

            // [x1, y1, x2, y2] -> [xc, yc, w1, h1]
            float xc = (x1 + x2) / 2.0;
            float yc = (y1 + y2) / 2.0;
            float w1 = x2 - x1;
            float h1 = y2 - y1;

            // rescale roi with regard to roi_scale and half_part
            float xx1 = xc - w1 * roi_scale / 2.0;
            float yy1 = yc - h1 * roi_scale / 2.0;
            float xx2 = xc + w1 * roi_scale / 2.0;
            float yy2 = yc + h1 * roi_scale / 2.0;

            // select subregion in roi
            switch (half_part) {
                case 0: break;
                case 1: xx2 = xc; break;  // left-half
                case 2: xx1 = xc; break;  // right-half
                case 3: yy2 = yc; break;  // top-half
                case 4: yy1 = yc; break;  // bottom-half
                case 5: xx2 = xc; yy2 = yc; break;  // left-top
                case 6: xx2 = xc; yy1 = yc; break;  // left-bottom
                case 7: xx1 = xc; yy2 = yc; break;  // right-top
                case 8: xx1 = xc; yy1 = yc; break;  // right-bottom
                default: break;
            }

            // rescaled roi size on conv featmap
            int roi_start_w = round(xx1 * spatial_scale + spatial_shift);
            int roi_start_h = round(yy1 * spatial_scale + spatial_shift);
            int roi_end_w = round(xx2 * spatial_scale + spatial_shift);
            int roi_end_h = round(yy2 * spatial_scale + spatial_shift);

            // Skip if ROI doesn't include (h, w)
            const bool in_roi = (w >= roi_start_w && w <= roi_end_w &&
                               h >= roi_start_h && h <= roi_end_h);
            if (!in_roi) {
                continue;
            }

            int offset = roi_n * pooled_height * pooled_width * channels;
            const float* offset_top_diff = top_diff + offset;
            const int* offset_argmax_data = argmax_data + offset;

            // Compute feasible set of pooled units that could have pooled
            // this bottom unit

            // Force malformed ROIs to be 1x1
            int roi_width = fmaxf(roi_end_w - roi_start_w + 1, 1);
            int roi_height = fmaxf(roi_end_h - roi_start_h + 1, 1);

            float bin_size_h = (float)(roi_height) / (float)(pooled_height);
            float bin_size_w = (float)(roi_width) / (float)(pooled_width);

            int phstart = floor((float)(h - roi_start_h) / bin_size_h);
            int phend = ceil((float)(h - roi_start_h + 1) / bin_size_h);
            int pwstart = floor((float)(w - roi_start_w) / bin_size_w);
            int pwend = ceil((float)(w - roi_start_w + 1) / bin_size_w);

            phstart = fminf(fmaxf(phstart, 0), pooled_height);
            phend = fminf(fmaxf(phend, 0), pooled_height);
            pwstart = fminf(fmaxf(pwstart, 0), pooled_width);
            pwend = fminf(fmaxf(pwend, 0), pooled_width);

            for (int ph = phstart; ph < phend; ++ph) {
                for (int pw = pwstart; pw < pwend; ++pw) {
                    if (offset_argmax_data[(c * pooled_height + ph) * pooled_width + pw] == index)
                    {
                        gradient += offset_top_diff[(c * pooled_height + ph) * pooled_width + pw];
                    }
                }
            }
        }
        bottom_diff[index] = gradient;
  }
}

int ROIMaskPoolBackwardLaucher(cudaStream_t stream,
                        float* bottom_diff, const float* bottom_rois, const float* top_diff, const int* argmax_data,
                        const int height, const int width, const int channels, const int batch_size,
                        const int pooled_height, const int pooled_width, const int num_rois,
                        const float roi_scale, const float mask_scale, const int half_part,
                        const float spatial_scale, const float spatial_shift) {

    const int kThreadsPerBlock = 1024;
    int output_size = batch_size * height * width * channels;
    cudaError_t err;

    ROIMaskPoolBackward<<<(output_size + kThreadsPerBlock - 1) / kThreadsPerBlock, kThreadsPerBlock, 0, stream>>>(
          output_size,
          top_diff, argmax_data, bottom_diff, bottom_rois,
          height, width, channels,
          pooled_height, pooled_width,
          roi_scale, mask_scale, half_part,
          spatial_scale, spatial_shift,
          num_rois);

    err = cudaGetLastError();
    if(cudaSuccess != err)
    {
        fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
        exit( -1 );
    }

    return 1;
}


// #ifdef __cplusplus
// }
// #endif
