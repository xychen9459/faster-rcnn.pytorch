#include <THC/THC.h>
#include <math.h>
#include "roi_mask_pooling_kernel.h"

extern THCState *state;

int roi_mask_pooling_forward_cuda(int pooled_height, int pooled_width, float spatial_scale,
                        float spatial_shift, int half_part, float roi_scale, float mask_scale,
                        THCudaTensor * features, THCudaTensor * rois, THCudaTensor * output, THCudaIntTensor * argmax,
                        THCudaTensor * return_rois)
{
    // Grab the input tensor
    float * data_flat = THCudaTensor_data(state, features);
    float * rois_flat = THCudaTensor_data(state, rois);

    float * output_flat = THCudaTensor_data(state, output);
    int * argmax_flat = THCudaIntTensor_data(state, argmax);
    float * return_rois_flat = THCudaTensor_data(state, return_rois);
    if (THCudaTensor_nDimension(state, return_rois) == 0) {
        return_rois_flat = NULL;
    }

    // Number of ROIs
    int num_rois = THCudaTensor_size(state, rois, 0);
    int size_rois = THCudaTensor_size(state, rois, 1);
    if (size_rois != 5) {
        return 0;
    }

    // batch size
    // int batch_size = THCudaTensor_size(state, features, 0);
    // if (batch_size != 1)
    // {
    //     return 0;
    // }
    // data height
    int data_height = THCudaTensor_size(state, features, 2);
    // data width
    int data_width = THCudaTensor_size(state, features, 3);
    // Number of channels
    int num_channels = THCudaTensor_size(state, features, 1);

    cudaStream_t stream = THCState_getCurrentStream(state);

    ROIMaskPoolForwardLaucher(stream,
                              data_flat, rois_flat, output_flat, argmax_flat,
                              return_rois_flat,
                              data_height, data_width, num_channels,
                              pooled_height, pooled_width, num_rois,
                              roi_scale, mask_scale, half_part,
                              spatial_scale, spatial_shift);

    return 1;
}

int roi_mask_pooling_backward_cuda(int pooled_height, int pooled_width, float spatial_scale,
                        float spatial_shift, int half_part, float roi_scale, float mask_scale,
                        THCudaTensor * top_grad, THCudaTensor * rois, THCudaTensor * bottom_grad, THCudaIntTensor * argmax)
{
    // Grab the input tensor
    float * top_grad_flat = THCudaTensor_data(state, top_grad);
    float * rois_flat = THCudaTensor_data(state, rois);

    float * bottom_grad_flat = THCudaTensor_data(state, bottom_grad);
    int * argmax_flat = THCudaIntTensor_data(state, argmax);

    // Number of ROIs
    int num_rois = THCudaTensor_size(state, rois, 0);
    int size_rois = THCudaTensor_size(state, rois, 1);
    if (size_rois != 5) {
        return 0;
    }

    // batch size
    int batch_size = THCudaTensor_size(state, bottom_grad, 0);
    // if (batch_size != 1)
    // {
    //     return 0;
    // }
    // data height
    int data_height = THCudaTensor_size(state, bottom_grad, 2);
    // data width
    int data_width = THCudaTensor_size(state, bottom_grad, 3);
    // Number of channels
    int num_channels = THCudaTensor_size(state, bottom_grad, 1);

    cudaStream_t stream = THCState_getCurrentStream(state);

    ROIMaskPoolBackwardLaucher(stream,
                               bottom_grad_flat, rois_flat, top_grad_flat, argmax_flat,
                               data_height, data_width, num_channels, batch_size,
                               pooled_height, pooled_width, num_rois,
                               roi_scale, mask_scale, half_part,
                               spatial_scale, spatial_shift);

    return 1;
}
