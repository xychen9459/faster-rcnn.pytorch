#!/bin/bash

GPU_ID=$1
NUM_GPUS=`echo ${GPU_ID} | awk -F, '{print NF}'`

if [[ ${NUM_GPUS} == 0 ]]; then
  echo "Please specify gpu id. Exit."
  exit 1
fi

EXTRA_ARGS=
if [[ ${NUM_GPUS} > 1 ]]; then
  echo "Run on multiple gpus."
  EXTRA_ARGS='--mGPUs'
fi

CUDA_VISIBLE_DEVICES=${GPU_ID} python trainval_net.py \
                                  --dataset coco \
                                  --net res101 \
                                  --bs 16 \
                                  --nw 1 \
                                  --lr 1e-2 \
                                  --lr_decay_step 4 \
                                  --epochs 6 \
                                  --cuda \
                                  --disp_interval 20 \
                                  --cfg ./cfgs/res101_align_coco.yml \
                                  --cag \
                                  ${EXTRA_ARGS}
