#!/bin/bash

GPU_ID=$1
NUM_GPUS=`echo ${GPU_ID} | awk -F, '{print NF}'`

if [[ ${NUM_GPUS} == 0 ]]; then
  echo "Please specify gpu id. Exit."
  exit 1
fi

CUDA_VISIBLE_DEVICES=${GPU_ID} python test_net.py \
                                  --arch rcnn \
                                  --dataset pascal_voc_0712 \
                                  --net res101 \
                                  --checksession 1 \
                                  --checkepoch 4 \
                                  --checkpoint 16550 \
                                  --cuda \
                                  --cfg ./cfgs/res101_align.yml \
                                  --cag
