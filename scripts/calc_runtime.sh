#!/bin/bash

LOGFILE=$1

if [[ "${LOGFILE}" != "" && -f "${LOGFILE}" ]]; then
# https://blog.csdn.net/csCrazybing/article/details/52594989
    cat ${LOGFILE} | grep -Eo 'time cost: [0-9.]+' \
      | sed 's/time\ cost:\ //' \
      | awk '{sum+=$1} END {print "Runtime = ", sum/3600, "h, AvgItertime = ", sum/NR, "s"}'
fi
