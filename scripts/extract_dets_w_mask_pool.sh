#!/bin/bash

ALL_SUBROI=(1 2 3 4 5 6 7 8)

for subroi in ${ALL_SUBROI[*]}
do
python extract_dets_w_mask_pool.py \
  --dataset pascal_voc_0712 \
  --net vgg16 \
  --checksession 1 \
  --checkepoch 6 \
  --checkpoint 33101 \
  --cuda \
  --cfg ./cfgs/vgg16_pool.yml \
  --vis \
  --subroi ${subroi}
done
