#!/bin/bash

GPU_ID=$1
NUM_GPUS=`echo ${GPU_ID} | awk -F, '{print NF}'`

if [[ ${NUM_GPUS} == 0 ]]; then
  echo "Please specify gpu id. Exit."
  exit 1
fi

CUDA_VISIBLE_DEVICES=${GPU_ID} python trainval_net_new.py \
                                  --dataset pascal_voc_0712 \
                                  --net res101 \
                                  --bs 2 \
                                  --nw 1 \
                                  --lr 1e-3 \
                                  --lr_decay_step 3 \
                                  --epochs 4 \
                                  --cuda \
                                  --disp_interval 20 \
                                  --cfg ./cfgs/res101_align.yml \
                                  --cag
